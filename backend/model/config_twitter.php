<?php

    $cnfg = parse_ini_file(MODEL_PATH."config_twitter.ini");
    $CONSUMER_KEY = $cnfg['CONSUMER_KEY'];
    $CONSUMER_SECRET = $cnfg['CONSUMER_SECRET'];
    $OAUTH_CALLBACK = $cnfg['OAUTH_CALLBACK'];        

    define('CONSUMER_KEY', $CONSUMER_KEY);
    define('CONSUMER_SECRET', $CONSUMER_SECRET);
    define('OAUTH_CALLBACK', $OAUTH_CALLBACK);

<?php

class controller_login {

    public function __construct() {


        include(FUNCTIONS_LOGIN . "functions.inc.php");
        include(UTILS . "upload.inc.php");

        $_SESSION['module'] = "login";
    }

    /////////////////////////////////////////////////// create_users
    public function alta_users() {

        $jsondata = array();

        $result = validate_users($_POST);

        if (($result['resultado'])) {
            $arrArgument = ($result['datos']['nombre']);

            set_error_handler('ErrorHandler');
            try {

                $exist = loadModel(MODEL_LOGIN, "login_model", "exist_users_nombre", $result['datos']['nombre']);
            } catch (Exception $e) {
                $jsondata['type_error'] = '503 ';
                $jsondata["success"] = false;
                $jsondata["message"] = "No se ha podido acceder a la bd";
                echo json_encode($jsondata);
                exit;
            }
            restore_error_handler();

            if ($exist[0]['total_user'] === '0') {


                $arrArgument = ($result['datos']['email']);

                set_error_handler('ErrorHandler');
                try {

                    $exist_mail = loadModel(MODEL_LOGIN, "login_model", "exist_users_email", $arrArgument);
                } catch (Exception $e) {
                    $jsondata['type_error'] = '503 ';
                    $jsondata["success"] = false;
                    $jsondata["message"] = "No se ha podido acceder a la bd";
                    echo json_encode($jsondata);
                    exit;
                }
                restore_error_handler();
                if (!$_POST['tipo']) {
                    $_POST['tipo'] = "normal";
                }
                if ($exist_mail[0]['total_email'] === '0') {

                    $_SESSION['avatar'] = get_gravatar(($result['datos']['email']), $s = 80, $d = 'identicon', $r = 'g', $img = false, $atts = array());


                    $arrArgument = array(
                        'nombre' => $result['datos']['nombre'],
                        'email' => ($result['datos']['email']),
                        'password' => password_hash($result['datos']['password'], PASSWORD_BCRYPT),
                        //'password2' =>($result['datos']['password2']),
                        'fecha_nac' => ($result['datos']['fecha_nac']),
                        'fecha_registro' => date("Y-m-d"),
                        'token' => md5(uniqid(rand(), true)),
                        'status' => ('0'),
                        'tipo' => $_POST['tipo'],
                        'avatar' => $_SESSION['avatar'],
                    );


                    set_error_handler('ErrorHandler');
                    try {

                        $arrValue = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
                    } catch (Exception $e) {
                        $jsondata['type_error'] = '503 ';
                        $jsondata["message"] = "No se ha podido crear el usuario en la bd";
                        $jsondata["success"] = false;

                        echo json_encode($jsondata);
                        exit;
                    }
                    restore_error_handler();

                    if ($arrValue) {
                        $arrValueEmail = array(
                            'type' => 'alta',
                            'inputName' => $result['datos']['nombre'],
                            'inputEmail' => $result['datos']['email'],
                            'token' => $arrArgument['token'],
                            'inputSubject' => 'Subject',
                            'inputMessage' => 'Alta usuario'
                        );

                        set_error_handler('ErrorHandler');
                        try {
                            if (enviar_email($arrValueEmail)) {
                                //$callback = "/main/begin";
                                $jsondata["success"] = true;
                                $jsondata["message"] = "Su mensaje a sido enviado, compruebe su bandeja de entrada";

                                //$jsondata["redirect"] = $callback;
                                $_SESSION['users'] = $arrArgument;
                                $_SESSION['token'] = $arrArgument['token'];



                                $jsondata["users"] = $_SESSION['users'];
                                echo json_encode($jsondata);
                                exit;

                                /* echo "<div class='alert alert-success'>Su mensaje a sido enviado, compruebe su bandeja de entrada</div>"; */
                            } else {
                                $jsondata['type_error'] = '503 ';
                                $jsondata["success"] = false;
                                $jsondata["message"] = "Error en el servidor";
                                echo json_encode($jsondata);
                                exit;
                                /* $jsondata["success"] = false;        
                                  echo json_encode($jsondata); */
                            }
                        } catch (Exception $e) {
                            /* $jsondata["success"] = false;
                              echo json_encode($jsondata); */
                            $jsondata['type_error'] = '503 ';
                            $jsondata["success"] = false;
                            $jsondata["message"] = "Error en el servidor";
                            echo json_encode($jsondata);
                            exit;
                        }
                        restore_error_handler();
                    } else {
                        $jsondata['type_error'] = '404 ';
                        $jsondata["success"] = false;
                        $jsondata["message"] = "Ha habido un problema en el servidor, por favor intentelo mas tarde";
                        /* header('HTTP/1.0 404 Bad error', true, 404); */
                        echo json_encode($jsondata);
                    }
                } else {
                    $jsondata["success"] = false;
                    $jsondata['type_error'] = '404 ';
                    $error['email'] = 'El email ya está registrado';
                    $result = array('resultado' => false, 'error' => $error, 'datos' => '');
                    $jsondata["error"] = $result['error'];

                    //header('HTTP/1.0 404 Bad error', true, 404);
                    echo json_encode($jsondata);
                }
            } else {

                $jsondata["success"] = false;
                $jsondata['type_error'] = '404 ';
                $error['nombre'] = 'El usuario ya existe';
                $result = array('resultado' => false, 'error' => $error, 'datos' => '');
                $jsondata["error"] = $result['error'];
                // header('HTTP/1.0 404 Bad error', true, 404);
                echo json_encode($jsondata);
            }
        } else {
            $jsondata['type_error'] = '404 ';
            $jsondata["success"] = false;
            $jsondata["error"] = $result['error'];

            //$jsondata["success1"] = false;
            //header('HTTP/1.0 404 Bad error', true, 404);
            echo json_encode($jsondata);
        }
    }

    ////////////////////////////////////////////signin

    public function signin() {


        $result = array();





        $result = login($_POST);


        if ($result['resultado']) {


            $arrArgument = $result['datos']['user'];
            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_LOGIN, "login_model", "exist_users_nombre", $arrArgument);
            } catch (Exception $e) {
                $jsondata["success"] = false;
                $jsondata["type_error"] = "503";
                echo json_encode($jsondata);
                exit;
            }
            restore_error_handler();


            //si existe el usuario
            if ($exist[0]['total_user'] === '1') {


                $arrArgument = $result['datos']['user'];

                try {
                    $user = loadModel(MODEL_LOGIN, "login_model", "details_users", $arrArgument);
                } catch (Exception $e) {



                    $jsondata["success"] = false;
                    $jsondata["type_error"] = "503";
                    echo json_encode($jsondata);
                    exit;
                }
                restore_error_handler();



                if ($user[0]['status'] === '1') {
                    $res = password_verify($result['datos']['password'], $user[0]['password']);



                


                    if ($res) {


                        $_SESSION['users'] = $user[0];

                        //$callback = SITE_PATH . "index.php?module=users&function=activarUser&aux=L" . $_SESSION['users']['token'];

                        $jsondata["success"] = true;
                        $jsondata["users"] = $_SESSION['users'];
                        //$jsondata["redirect"] = $callback;
                        echo json_encode($jsondata);
                        exit;
                    } else {



                        $jsondata["success"] = false;

                        $error['password'] = 'Contraseña incorrecta';
                        $result = array('resultado' => false, 'error' => $error, 'datos' => '');
                        $jsondata["error"] = $result['error'];
                        $jsondata['type_error'] = '404 ';
                        //header('HTTP/1.0 404 Bad error', true, 404);
                        echo json_encode($jsondata);
                    }
                } else {
                    $jsondata["success"] = false;

                    $error['activar'] = 'El usuario no está activado';
                    $result = array('resultado' => false, 'error' => $error, 'datos' => '');
                    $jsondata["error"] = $result['error'];
                    $jsondata['type_error'] = '404 ';
                    //header('HTTP/1.0 404 Bad error', true, 404);
                    echo json_encode($jsondata);
                }
            } else {


                $jsondata["success"] = false;

                $error['user'] = 'El usuario no está registrado';
                $jsondata['type_error'] = '404 ';
                $result = array('resultado' => false, 'error' => $error, 'datos' => '');
                $jsondata["error"] = $result['error'];
                // header('HTTP/1.0 404 Bad error', true, 404);
                echo json_encode($jsondata);
            }
        } else {
            $jsondata['type_error'] = '404 ';
            $jsondata["success"] = false;
            $jsondata["error"] = $result['error'];
            // header('HTTP/1.0 404 Bad error', true, 404);
            echo json_encode($jsondata);
        }
    }

    public function social_facebook() {

        if ($_POST) {

            set_error_handler('ErrorHandler');
            try {

                $exist = loadModel(MODEL_LOGIN, "login_model", "users_exist_codigo", $_POST['id']);
            } catch (Exception $e) {
                $jsondata["success"] = false;
                $jsondata["type_error"] = "503";
                echo json_encode($jsondata);
                exit;
            }
            restore_error_handler();


            if ($exist[0]['total_codigo'] === '0') {

                if (isset($_POST['picture'])) {
                    
                } else {
                    $_POST['picture']['data']['url'] = get_gravatar(($result['datos']['email']), $s = 80, $d = 'identicon', $r = 'g', $img = false, $atts = array());
                }
                $_POST['password'] = "";
                $arrArgument = array(
                    'nombre' => $_POST['name'],
                    'email' => ($_POST['email']),
                    'fecha_registro' => date("Y-m-d"),
                    'token' => md5(uniqid(rand(), true)),
                    'status' => ('1'),
                    'tipo' => ('normal'),
                    'avatar' => $_POST['picture']['data']['url'],
                    'codigo' => $_POST['id'],
                    'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
                );



                set_error_handler('ErrorHandler');
                try {

                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
                } catch (Exception $e) {
                    $jsondata["success"] = false;
                    $jsondata["type_error"] = "503";
                    echo json_encode($jsondata);
                    exit;
                }
                restore_error_handler();

                if ($arrValue) {


                    $_SESSION['users'] = $arrArgument;
                    $_SESSION['token'] = $arrArgument['token'];

                    $callback = SITE_PATH . "index.php?module=users&function=activarUser&aux=A" . $_SESSION['users']['token'];


                    $jsondata["success"] = true;
                    $jsondata["users"] = $_SESSION['users'];
                    $jsondata["redirect"] = $callback;
                    echo json_encode($jsondata);
                }
            } else {

                set_error_handler('ErrorHandler');
                try {

                    $arrUsers = loadModel(MODEL_LOGIN, "login_model", "users_exist", $_POST['name']);
                } catch (Exception $e) {
                    $jsondata["success"] = false;
                    $jsondata["type_error"] = "503";
                    echo json_encode($jsondata);
                    exit;
                }
                restore_error_handler();

                if ($arrUsers) {


                    $_SESSION['users'] = $arrUsers[0];
                    $_SESSION['token'] = $arrUsers['token'];

                    $jsondata['avatar'] = $_POST['picture']['data']['url'];
                    $callback = SITE_PATH . "index.php?module=users&function=activarUser&aux=L" . $_SESSION['users']['token'];
                    $jsondata["users"] = $_SESSION['users'];
                    $jsondata["redirect"] = $callback;

                    $jsondata["success"] = true;
                    echo json_encode($jsondata);
                    exit;
                } else {

                    $jsondata["success"] = false;
                    $jsondata["error"] = "No se ha encontrado al usuario";

                    $jsondata["success1"] = false;
                    if ($result_avatar['resultado']) {
                        $jsondata["success1"] = true;
                    }
                    echo json_encode($jsondata);
                }
            }
        } else {

            $jsondata["success"] = false;
            $jsondata["error"] = $result['error'];

            $jsondata["success1"] = false;
            if ($result_avatar['resultado']) {
                $jsondata["success1"] = true;
            }



            echo json_encode($jsondata);
        }
    }

    public function social_twitter() {

        if ($_POST) {

            set_error_handler('ErrorHandler');
            try {

                $exist = loadModel(MODEL_LOGIN, "login_model", "users_exist_codigo", $_POST['id']);
            } catch (Exception $e) {
                $jsondata["success"] = false;
                $jsondata["type_error"] = "503";
                echo json_encode($jsondata);
                exit;
            }
            restore_error_handler();



            if ($exist[0]['total_codigo'] === '0') {

                if (isset($_POST['profile_image_url'])) {
                    
                } else {
                    $_POST['profile_image_url'] = get_gravatar(($_POST['id']), $s = 80, $d = 'identicon', $r = 'g', $img = false, $atts = array());
                }
                $_POST['password'] = "";
                $arrArgument = array(
                    'nombre' => $_POST['name'],
                    'email' => ($_POST['email']),
                    'fecha_registro' => date("Y-m-d"),
                    'token' => md5(uniqid(rand(), true)),
                    'status' => ('1'),
                    'tipo' => ('normal'),
                    'avatar' => $_POST['profile_image_url'],
                    'codigo' => $_POST['id'],
                    'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
                );



                set_error_handler('ErrorHandler');
                try {

                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
                } catch (Exception $e) {
                    $jsondata["success"] = false;
                    $jsondata["type_error"] = "503";
                    echo json_encode($jsondata);
                    exit;
                }
                restore_error_handler();

                if ($arrValue) {

                    $_SESSION['users'] = $arrArgument;
                    $_SESSION['token'] = $arrArgument['token'];


                    $callback = SITE_PATH . "index.php?module=users&function=activarUser&aux=A" . $_SESSION['users']['token'];

                    $jsondata["success"] = true;
                    $jsondata["users"] = $_SESSION['users'];
                    $jsondata["redirect"] = $callback;
                    echo json_encode($jsondata);
                }
            } else {

                set_error_handler('ErrorHandler');
                try {

                    $arrUsers = loadModel(MODEL_LOGIN, "login_model", "users_exist", $_POST['name']);
                } catch (Exception $e) {
                    $jsondata["success"] = false;
                    $jsondata["type_error"] = "503";
                    echo json_encode($jsondata);
                    exit;
                }
                restore_error_handler();

                if ($arrUsers) {


                    $_SESSION['users'] = $arrUsers[0];
                    $_SESSION['token'] = $arrUsers['token'];

                    $jsondata['avatar'] = $_POST['profile_image_url'];
                    $callback = SITE_PATH . "index.php?module=users&function=activarUser&aux=L" . $_SESSION['users']['token'];
                    $jsondata["users"] = $_SESSION['users'];
                    $jsondata["redirect"] = $callback;

                    $jsondata["success"] = true;
                    echo json_encode($jsondata);
                    exit;
                } else {

                    $jsondata["success"] = false;
                    $jsondata["error"] = "No se ha encontrado al usuario";

                    $jsondata["success1"] = false;
                    if ($result_avatar['resultado']) {
                        $jsondata["success1"] = true;
                    }
                    echo json_encode($jsondata);
                }
            }
        } else {

            $jsondata["success"] = false;
            $jsondata["error"] = $result['error'];

            $jsondata["success1"] = false;
            if ($result_avatar['resultado']) {
                $jsondata["success1"] = true;
            }


            header('HTTP/1.0 404 Bad error', true, 404);
            echo json_encode($jsondata);
        }
    }

    /////////////////////////////////////////////////// Recovery
    public function email_recovery() {
        /* Comparamos el email, para ver si existe */


        $jsondata = array();


        /* //////////////////////////////////////////recogemos datos por $_POST['email'] */
        /* $email_recovery_JSON = json_decode($_POST["recovery_email_JSON"], true);        */
        /* $email_recovery_JSON = json_decode($_POST, true); */
        /* header('HTTP/1.0 404 Bad error', true, 404); */

        /* $result = validatemail($email_recovery_JSON['email']); */
        $result = validatemail($_POST['email']);


        if ($result) {
            $token = false;
            set_error_handler('ErrorHandler');
            try {

                /* $arrValue = loadModel(MODEL_LOGIN, "login_model", "validate_email_recover", $email_recovery_JSON['email']); */
                $token = loadModel(MODEL_LOGIN, "login_model", "validate_email_recover", $_POST['email']);
           
                
               
                
            } catch (Exception $e) {
                $token = false;
            }
            restore_error_handler();
        } else {
            $jsondata["success"] = false;
            $jsondata["mensaje"] = "El email introducido no es valido";
            /* header('HTTP/1.0 404 Bad error', true, 404); */
            echo json_encode($jsondata);
        }

        /* Actualizamos el token, y enviamos email */
        if ($token) {

  

            $pass = substr(str_shuffle(str_repeat("0123456789ABCDEFGHHIJKLMNLOPQRST", 5)), 0, 5);
            $pass_1 = substr("abcdefghijklmnopqrstuvwxyz", mt_rand(0, 8), 1) . substr(md5(time()), 1);
            $PASSf = $pass . $pass_1;

            $pass_crypt = password_hash($PASSf, PASSWORD_BCRYPT);

            $arr_recovery_pass = array(
                'pass' => $pass_crypt,
                'email' =>  $_POST['email'],
            );
           
    
            set_error_handler('ErrorHandler');
            try {

                $arrValue_ = loadModel(MODEL_LOGIN, "login_model", "update_pass_recover", $arr_recovery_pass);
            
                
            } catch (Exception $e) {
                $arrValue_ = false;
            }
            restore_error_handler();

            if ($arrValue_) {

                $arr_recovery = array(
                    /* 'email' => $email_recovery_JSON['email'], */
                    'email' => $_POST['email'],
                    'token' => $token[0]['token'],
                    'password' => $PASSf,
                );




                $arrArgument = array(
                    'type' => 'recovery',
                    'token' => $arr_recovery['token'],
                    'inputName' => 'Name',
                    'password' => $arr_recovery['password'],
                    'inputEmail' => $arr_recovery['email'],
                    'inputSubject' => 'Subject',
                    'inputMessage' => 'Message'
                );

                set_error_handler('ErrorHandler');
                try {
                    if (enviar_email($arrArgument)) {
                        //$callback = "/main/begin";
                        $jsondata["success"] = true;
                        $jsondata["mensaje"] = "Su mensaje ha sido enviado, compruebe su bandeja de entrada";
                        //$jsondata["redirect"] = $callback;
                        echo json_encode($jsondata);
                        /* echo "<div class='alert alert-success'>Su mensaje a sido enviado, compruebe su bandeja de entrada</div>"; */
                    } else {
                        /* $jsondata["success"] = false;        
                          echo json_encode($jsondata); */
                        echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
                    }
                } catch (Exception $e) {
                    /* $jsondata["success"] = false;
                      echo json_encode($jsondata); */
                    echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
                }
                restore_error_handler();
            } else {
                $jsondata["success"] = false;
                $jsondata["mensaje"] = "Ha habido un problema en el servidor, por favor intentelo mas tarde";
                /* header('HTTP/1.0 404 Bad error', true, 404); */
                echo json_encode($jsondata);
            }
        } else {
            $jsondata["success"] = false;
            $jsondata["mensaje"] = "El email introducido no existe";
            /* header('HTTP/1.0 404 Bad error', true, 404); */
            echo json_encode($jsondata);
        }
    }

    /////////////////////////////////////////////////// Recovery

    public function email_recovery_pass() {

        $jsondata = array();



        /* $email_recovery_JSON = json_decode($_POST["data_pass_JSON"], true); */
        /* $pass = $email_recovery_JSON['password'];
          $pass2 = $email_recovery_JSON['password2']; */
        $pass = $_POST['password'];
        $pass2 = $_POST['password2'];



        $result = validar_passwd($pass, $pass2);

        if ($result == "") {

            $pass_crypt = password_hash($pass, PASSWORD_BCRYPT);

            $arr_recovery_pass = array(
                'pass' => $pass_crypt,
                'token' => $_SESSION['users'][0]['token'],
            );

            set_error_handler('ErrorHandler');
            try {

                $arrValue = loadModel(MODEL_LOGIN, "login_model", "update_pass_recover", $arr_recovery_pass);
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();

            if ($arrValue) {

                /* $callback = "/login/create_users"; */
                $jsondata["success"] = true;
                $jsondata["mensaje"] = "Se ha modificado su contraseña satisfactoriamente";
                /* $jsondata["redirect"] = $callback; */
                echo json_encode($jsondata);
            } else {
                $jsondata["success"] = false;
                $jsondata["mensaje"] = "Ha habido un problema en el servidor, porfavor intentelo más tarde";
                echo json_encode($jsondata);
            }
        } else {
            $jsondata["success"] = false;
            $jsondata["mensaje"] = $result;
            echo json_encode($jsondata);
        }
    }

    //////////////////////////////////////////////////////////////// close_sesion
    function logout() {

        session_destroy();
        redirect(amigable("?module=main&function=begin", true));
    }

/////////////************enviar email**********/////////

    public function process_contact() {
        if ($_POST['token'] === "formulario") {

//////////////// Envio del correo al usuario
            $arrArgument = array(
                'type' => 'alta',
                'token' => $_SESSION['token'],
                'inputName' => $_POST['nombre'],
                'inputEmail' => $_POST['email'],
            );


            set_error_handler('ErrorHandler');
            try {
                if (enviar_email($arrArgument)) {
//echo($arrArgument);

                    echo("Consulte su bandeja de entrada para finalizar registro");
                } else {
                    echo ("Server error. Intentar más tarde...");
                }
            } catch (Exception $e) {
                echo "Server error. Intentar más tarde...";
            }
            restore_error_handler();


//////////////// Envio del correo al admin de la ap web
            $arrArgument = array(
                'type' => 'admin',
                'token' => $_SESSION['token'],
                'inputName' => $_POST['nombre'],
                'inputEmail' => $_POST['email'],
            );
            set_error_handler('ErrorHandler');
            try {
                if (enviar_email($arrArgument)) {

//  echo "<div class='alert alert-success'>Your message has been sent </div>";
                } else {
                    echo "<div class='alert alert-error'>Server error. Try later...</div>";
                }
            } catch (Exception $e) {
                echo "<div class='alert alert-error'>Server error. Try later...</div>";
            }
            restore_error_handler();
        } else {
            echo "<div class='alert alert-error'>Server error. Try later...</div>";
        }
    }

}

<?php

//require(BLL_USERS . "userBLL.class.singleton.php");
    
class login_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = login_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_user($arrArgument) {
         
        return $this->bll->create_user_BLL($arrArgument);
    }

    public function obtain_paises($url) {
        return $this->bll->obtain_paises_BLL($url);
    }

    public function obtain_provincias() {
        return $this->bll->obtain_provincias_BLL();
    }

    public function obtain_poblaciones($arrArgument) {
        return $this->bll->obtain_poblaciones_BLL($arrArgument);
    }
      public function exist_users_nombre($arrArgument) {
   
       return $this->bll->exist_users_nombre_BLL($arrArgument);
    }
    
      public function details_users($arrArgument) {
       
       return $this->bll->details_users_BLL($arrArgument);
    }
    
      public function exist_users_email($arrArgument) {
       return $this->bll->exist_users_email_BLL($arrArgument);
    }

    public function validate_email_recover($arrArgument) {
        
       return $this->bll->validate_email_recover_BLL($arrArgument);
    }
    
    public function update_email_recover($arrArgument) {
       return $this->bll->update_email_recover_BLL($arrArgument);
    }
    
    public function update_pass_recover($arrArgument) {
       return $this->bll->update_pass_recover_BLL($arrArgument);
    }
     
    public function users_exist($arrArgument) {
      
       return $this->bll->users_exist_BLL($arrArgument);
    }
    
      public function users_exist_codigo($arrArgument) {
               

       return $this->bll->users_exist_codigo_BLL($arrArgument);
    }
    
    
}

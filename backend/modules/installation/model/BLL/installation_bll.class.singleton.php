<?php
//require (MODEL_PATH . "Db.class.singleton.php");
//require(DAO_INSTALLATION . "installDAO.class.singleton.php");

class installation_bll {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = installation_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    /* public function create_install_BLL($arrArgument) {
      return $this->dao->create_install_DAO($this->db, $arrArgument);
      } */

    public function list_install_BLL() {

        return $this->dao->list_install_DAO($this->db);
    }

    public function details_install_BLL($id) {

        return $this->dao->details_install_DAO($this->db, $id);
    }

    public function order_install_BLL($arrArgument) {


        return $this->dao->order_install_DAO($this->db, $arrArgument);
    }

    public function count_install_BLL() {


        return $this->dao->count_install_DAO($this->db);
    }

    public function order_install_nombre_BLL() {


        return $this->dao->order_install_nombre_DAO($this->db);
    }

    public function filter_install_nombre_BLL($arrArgument) {


        return $this->dao->filter_install_nombrel_DAO($this->db, $arrArgument);
    }

    public function count_install__criteria_BLL($arrArgument) {


        return $this->dao->count_install_criteria_DAO($this->db, $arrArgument);
    }

    public function order_install_criteria_BLL($arrArgument) {


        return $this->dao->order_install_criteria_DAO($this->db, $arrArgument);
    }

}

<?php
class controller_games {    
    /*estamos realizando la parte de coordenadas, ya funciona pero vamos a ver como registramos las partidas y crear la funcionalidad de filtrar las instalaciones*/
    function __construct(){        
        include(FUNCTIONS_GAME."functions.inc.php");/*es el modulo necesario*/        
    }    
    

    function up_games() {
    
        //if (isset($_POST['data_games_JSON'])) {
        //if (isset($_POST['name'])) {
        if ($_POST['usuario'] && $_POST['zona'] && $_POST['contact_install']) {
           
            $jsondata = array();

            //$usersJSON = json_decode($_POST["data_games_JSON"], true);
            $usersJSON = $_POST;
            /*$jsondata["mensaje"] = $usersJSON;
            echo json_encode($jsondata);
            exit;*/

            
            $result = validate_game($usersJSON);//name, time, duration, places, day, pricecash, sport, 
            
            $coor = search_coor($usersJSON);
            $id = $coor[0][id];            
            
            /*estamos debugueando esto para poner coordenadas encontradas o por defecto*/
            
            
                /*$provincia = $result['datos']['provincia'];
                $poblacion = $result['datos']['poblacion'];
                $coor = coordenadas($provincia, $poblacion);
                $latitud = $coor[0];
                $longitud = $coor[1];      */                         
            
                //$latitud = $coor[0][latitud];
                //$longitud = $coor[0][longitud];
                /*necesitamos modifica los campos de la base de datos*/
                if (($result['resultado'])) {

                    $arrArgument = array(
                        'name' => strtoupper($result['datos']['name']),
                        'time' => $result['datos']['time'],
                        'duration' => $result['datos']['duration'],
                        'pricecash' => $result['datos']['pricecash'],
                        'places' => $result['datos']['places'],
                        'day' => strtoupper($result['datos']['day']),
                        'sport' => $result['datos']['sport'],
                        'usuario' => $usersJSON['usuario'],
                        'zona' => $usersJSON['zona'],
                        //'provincia' => $result['datos']['provincia'],
                        //'poblacion' => $result['datos']['poblacion'],
                        //'latitud' => $latitud,
                        //'longitud' => $longitud,
                        'id_install' => $id,
                        'install_name' => $usersJSON['contact_install']
                     );
                   
                    //$_SESSION['result_avatar'] = array();

                    /* insert into BD begin */
                    $arrValue = false;
                    //set_error_handler('ErrorHandler');/*Nos esta creando errores, no podemos pasar de esta linea, nos crea error*/

                    try {

                        $arrValue = loadModel(MODEL_GAME, "game_model", "create_game", $arrArgument);
                    } catch (Exception $ex) {

                        $arrValue = false;
                    }
                    
                    /* insert into BD end */
                    if ($arrValue) {
                        $mensaje = "Su Partida se ha regitrado correctamente";                        
                        $jsondata['mensaje'] = $mensaje;                       
                        $jsondata["success"] = true;                        
                        echo json_encode($jsondata);
                        exit;
                    } else {
                        $mensaje = "No se ha podido realizar el alta de la partida. Porfavor intentelo mas tarde";
                        $jsondata["success"] = false;
                        $jsondata["mensaje"] = $mensaje;
                        echo json_encode($jsondata);
                        exit;
                        
                    }
                } else {
                    $jsondata["success"] = false;
                    $jsondata["mensaje"] = $result['error']['error'];                    
                    echo json_encode($jsondata);
                    exit;
                }                        
            
            //no gastar header
        }else{
            $mensaje = "Revise todos los campos porfavor";
            $jsondata["success"] = false;
            $jsondata["mensaje"] = $mensaje;
            echo json_encode($jsondata);
            exit;
        }
    }
    
    function up_install() {

       
       

        if ($_POST['name'] && $_POST['poblacion'] && $_POST['longitud'] && $_POST['latitud'] && $_POST['descripcion'] && $_POST['valoracion']) {
            
            
            $avatar = 'installa.png';

            $arrArgument = array(
                'name' => strtoupper($_POST['name']),
                'poblacion' => $_POST['poblacion'],
                'longitud' => $_POST['longitud'],
                'latitud' => $_POST['latitud'],
                'descripcion' => $_POST['descripcion'],
                'valoracion' => $_POST['valoracion'],
                'avatar' => $avatar
            );


            $arrValue = false;
            //set_error_handler('ErrorHandler');/*Nos esta creando errores, no podemos pasar de esta linea, nos crea error*/

            try {

                $arrValue = loadModel(MODEL_GAME, "game_model", "create_ins", $arrArgument);
            } catch (Exception $ex) {

                $arrValue = false;
            }
                        
            
            if ($arrValue) {
                $mensaje = "Su instalacion se ha regitrado correctamente";
                $jsondata['mensaje'] = $mensaje;
                $jsondata["success"] = true;
                echo json_encode($jsondata);
                exit;
            } else {
                $mensaje = "No se ha podido realizar el alta de la instalacion. Porfavor intentelo mas tarde";
                $jsondata["success"] = false;
                $jsondata["mensaje"] = $mensaje;
                echo json_encode($jsondata);
                exit;
            }
        } else {
            $mensaje = "Revise todos los campos porfavor";
            $jsondata["success"] = false;
            $jsondata["mensaje"] = $mensaje;
            echo json_encode($jsondata);
            exit;
        }
    }

    function autocomplete_installation_population() {
        
        
        
        /*if ($_POST["data_games_pob_JSON"]) {*/
        if ($_POST["poblacion"]) {
            //$criteria = json_decode($_POST["data_games_pob_JSON"], true);
            $criteria = $_POST["poblacion"];

            set_error_handler('ErrorHandler');

            try {
//loadmodel
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/installation/model/model/';
                $results = loadModel(MODEL_GAME, "game_model", "filter_install_nombre", $criteria);//$criteria['poblacion']


//throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                $jsondata["success"] = false;
                $jsondata["mensaje"] = "Hay un problema en la consulta a base de datos, porfavor intentelo mas tarde";
                echo json_encode($jsondata);
                exit;
                /* showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503); */
            }
            restore_error_handler();

            if ($results) {
                $jsondata["success"] = true;
                $jsondata["ubucacion_install"] = $results;
                echo json_encode($jsondata);
                exit;
            } else {
//if($nom_installationos){ //que lance error si no hay installationos
                $jsondata["success"] = false;
                $jsondata["mensaje"] = "No se ha encontrado ninguna intalacion en esta poblacion";
                echo json_encode($jsondata);
                exit;
            }
        }
    }
    
    
    function latlang(){
        
        
        
        $provincia = $result['datos']['provincia'];
        $poblacion = $result['datos']['poblacion'];
        $coor = coordenadas($provincia, $poblacion);
        $latitud = $coor[0];
        $longitud = $coor[1];                             

        $latitud = $coor[0][latitud];
        $longitud = $coor[0][longitud];
        $jsondata["success"] = true;
        $jsondata["coor"] = $coor;
        echo json_encode($jsondata);
        exit;
        
    }
    

    function delete(){
        /* delete */
        if (isset($_POST["delete"]) && $_POST["delete"] == true) {
            $_SESSION['result_avatar'] = array();
            $result = remove_files();
            if ($result === true) {
                echo json_encode(array("res" => true));
            } else {
                echo json_encode(array("res" => false));
            }
        }
    }
    

    function load_game(){         
        /* load */
        if (isset($_POST["load_game"]) && $_POST["load_game"] == true) {
            $jsondata = array();
            if (isset($_SESSION['game'])) {
                //echo debug($_SESSION['user']);
                $jsondata['game'] = $_SESSION['game'];
            }
            if (isset($_SESSION['msje'])) {
                //echo $_SESSION['msje'];
                $jsondata["msje"] = $_SESSION['msje'];
            }
            
            if($jsondata){
                close_session(); /*en utils/utils.inc.php*/
                echo json_encode($jsondata);
                exit;
            }else{
                close_session(); /*en utils/utils.inc.php*/
                showErrorPage(1, "", 'HTTP/1.0 404 Not Found', 404);
            }            
        }
    }

    function load_data_game(){
        /* load data */
        if ((isset($_POST["load_data_game"])) && ($_POST["load_data_game"] == true)) {
            $jsondata = array();

            if (isset($_SESSION['game'])) {
                $jsondata["prod"] = $_SESSION['game'];
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["prod"] = "";
                echo json_encode($jsondata);
                exit;
            }
        }
    }
    

    function load_countries(){
        /* load countries */
        if ((isset($_POST["load_pais"])) && ($_POST["load_pais"] == true)) {
            $json = array();

            $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';
            try {
                
                $json = loadModel(MODEL_FORM, "prodModel", "obtain_countries", $url);
            } catch (Exception $ex) {
                $json = array();
            }


            if ($json) {
                echo $json;
                exit;
            } else {
                $json = "error";
                echo $json;
                exit;
            }
        }
    }
   
    function load_provinces(){
        
        /* load provinces */
        if ((isset($_POST["load_provincias"])) && ($_POST["load_provincias"] == true)) {            
            $jsondata = array();
            $json = array();

            try {
                
                $json = loadModel(MODEL_GAME, "game_model", "obtain_provinces");
            } catch (Exception $ex) {
                $json = array();
            }


            if ($json) {
                $jsondata["provincias"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["provincias"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        }  
    }
    

    function load_populations(){
        /* load populations */
        if (isset($_POST['idPoblac'])) {
            $jsondata = array();
            $json = array();

            try {
                
                $json = loadModel(MODEL_GAME, "game_model", "obtain_populations", $_POST['idPoblac']);
            } catch (Exception $ex) {
                $json = array();
            }


            if ($json) {
                $jsondata["poblaciones"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["poblaciones"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        } 
    }    
    
    /*function coordenadas($provincia,$poblacion){

    $direccion = $provincia.", ".$poblacion;
    /*nom hemos kedado mirando la funcionalidad de esta funcion
    $direccion_google = 'Calle, Población, Provincia / Estado, País';
    $resultado = file_get_contents(sprintf('https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=%s', urlencode($direccion_google)));
    $resultado = json_decode($resultado, TRUE);

    $lat = $resultado['results'][0]['geometry']['location']['lat'];
    $lng = $resultado['results'][0]['geometry']['location']['lng'];

    $coor = Array($lat, $lng); 
    echo debugPHP ($coor);                
    die();
    return $coor;
    }*/
    
    /////////////////////////////////////////details game
    public function idGame() {

        if ($_GET["id"]) {


            $result = filter_num_int($_GET["id"]);

            if ($result['resultado']) {

                $id = $result['datos'];
            } else {
                $id = 1;
            }
            set_error_handler('ErrorHandler');
            try {
// throw new Exception(); //para probar que entre en el catch
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_5/modules/installation/model/model/';
                $game = loadModel(MODEL_GAME, "game_model", "details_game", $id);
            } catch (Exception $e) {

                $jsondata["success"] = false;
                $jsondata["type_error"] = "503";
                echo json_encode($jsondata);
                exit;
            }
            restore_error_handler();

            if ($game) {
                $jsondata["success"] = true;
//require_once 'modules/services/view/details_services.php';
//loadView('modules/installation/view/', 'details_installations.php', $install[0]);
                $jsondata["game"] = $game[0];
                echo json_encode($jsondata);
                exit;
            } else {

                $jsondata["success"] = false;
                $jsondata["type_error"] = "404";
                $jsondata["mensaje"] = "No se ha podido obtener la partida";
                echo json_encode($jsondata);
                exit;
            }
        }
    }
}

    
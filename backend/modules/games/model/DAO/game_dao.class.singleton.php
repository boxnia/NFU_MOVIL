<?php

class game_dao {

    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_game_DAO($db, $arrArgument) {
        
        
        $id = null;
        $name = $arrArgument['name'];
        $usu = $arrArgument['usuario'];
        $zona = $arrArgument['zona'];
        $time = $arrArgument['time'];
        $duration = $arrArgument['duration'];
        $pricecash = $arrArgument['pricecash'];
        $places = $arrArgument['places'];
        $day = $arrArgument['day'];
        $sport = $arrArgument['sport'];
        $id_install = $arrArgument['id_install'];
        $install_name = $arrArgument['install_name'];
        
        
        
        
        /*Asi es como quedara la base de datos con las poblaciones, no lo hemos hecho todavia porque estamos en pruebas*/
        /*$sql = "INSERT INTO game (id, nombre, ubicacion, deporte,"
                . " hora, duracion, inscripcion, plazas, dia)"
                . " VALUES ('$id', '$name', '$ubicacion',"
                . " '$sport', '$time', '$duration', '$pricecash', '$places', '$day')";      */       
        $sql = "INSERT INTO game (id, nombre_game, usuario, zona, deporte,"
                . " hora, duracion, inscripcion, plazas, dia, id_install, install_name)"
                . " VALUES ('$id', '$name', '$usu', '$zona',"
                . " '$sport', '$time', '$duration', '$pricecash', '$places', '$day', '$id_install', '$install_name')";
        return $db->ejecutar($sql);
    }

    
    public function create_ins_DAO($db, $arrArgument){
        
        $id = null;
        $name = $arrArgument['name'];
        $ubicacion = $arrArgument['poblacion'];
        $longitud = $arrArgument['longitud'];
        $latitud = $arrArgument['latitud'];
        $descripcion = $arrArgument['descripcion'];
        $valoracion = $arrArgument['valoracion'];
        $avatar = $arrArgument['avatar'];
        
        
        $sql = "INSERT INTO installation (id, nombre, ubicacion, descripcion, valoracion,"
                . " avatar, latitud, longitud)"
                . " VALUES ('$id', '$name', '$ubicacion', '$descripcion',"
                . " '$valoracion', '$avatar', '$latitud', '$longitud')";
        return $db->ejecutar($sql);
        
    }
    
    
    
    
    /*public function obtain_countries_DAO($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $file_contents = curl_exec($ch);
        curl_close($ch);

        return ($file_contents) ? $file_contents : FALSE;
    }*/

    public function obtain_provinces_DAO() {
        $json = array();
        $tmp = array();

        /*$provincias = simplexml_load_file("../../../../resources/provinciasypoblaciones.xml");*/
        $provincias = simplexml_load_file(SITE_ROOT."resources/provinciasypoblaciones.xml");
        $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
        for ($i = 0; $i < count($result); $i+=2) {
            $e = $i + 1;
            $provincia = $result[$e];

            $tmp = array(
                'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
        }
        return $json;
    }

    public function obtain_populations_DAO($arrArgument) {
        $json = array();
        $tmp = array();

        $filter = (string) $arrArgument;
        /*$xml = simplexml_load_file('../../../../resources/provinciasypoblaciones.xml');*/
        $xml = simplexml_load_file(SITE_ROOT."resources/provinciasypoblaciones.xml");
        $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

        for ($i = 0; $i < count($result[0]); $i++) {
            $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
            );
            array_push($json, $tmp);
        }
        return $json;
    }
    
    public function filter_install_nombre_DAO($db, $criteria) {
        
        $sql = "SELECT * FROM installation WHERE ubicacion='$criteria'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }
    
    public function get_coor_install_DAO($db, $criteria) {
                
        //$sql = "SELECT latitud, longitud, id FROM installation WHERE ubicacion='$criteria[poblacion]'and nombre = '$criteria[instalacion]'";
        $sql = "SELECT id FROM installation WHERE ubicacion='$criteria[poblacion]'and nombre = '$criteria[instalacion]'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }
    
      public function details_game_DAO($db, $id) {

        $sql = "SELECT * FROM game WHERE id='$id'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }


}

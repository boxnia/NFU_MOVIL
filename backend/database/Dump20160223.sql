CREATE DATABASE  IF NOT EXISTS `nfu` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `nfu`;
-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: nfu
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `nombre_game` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `usuario` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `zona` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `deporte` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `hora` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `duracion` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inscripcion` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `plazas` mediumint(9) DEFAULT NULL,
  `dia` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `id_install` mediumint(8) NOT NULL,
  `install_name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_install` (`id_install`),
  CONSTRAINT `game_ibfk_1` FOREIGN KEY (`id_install`) REFERENCES `installation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
INSERT INTO `game` VALUES (1,'TENISTOTAL','ontinyent',NULL,'./backend/media/Tenis.svg','08:00 AM','01:00:00','Gratuio',1,'01/08/2016',1,NULL),(2,'TENISTOTAL','ontinyent',NULL,'./backend/media/Tenis.svg','08:00 AM','01:00:00','Gratuio',1,'01/08/2016',2,NULL),(3,'VOLEY ONTINYENT','ontinyent',NULL,'./backend/media/Volei.svg','09:00 AM','01:00:00','Gratuio',5,'01/08/2016',1,NULL),(4,'FUTBOL A TOPE','ontinyent',NULL,'./backend/media/Futbol.svg','08:00 AM','01:00:00','Gratuio',7,'01/08/2016',3,NULL),(5,'FUTBOLTOTAL','ontinyent',NULL,'./backend/media/Futbol.svg','08:00 AM','01:00:00','Gratuio',7,'01/08/2016',4,NULL),(6,'FUTBOLTOTAL','ontinyent',NULL,'./backend/media/Futbol.svg','08:00 AM','01:00:00','Gratuio',7,'01/08/2016',5,NULL),(7,'FUTBOL TOTAL','ontinyent',NULL,'./backend/media/Futbol.svg','08:00 AM','01:00:00','Gratuio',7,'01/09/2016',5,NULL),(8,'BALONCESTO A TOPE','ontinyent',NULL,'./backend/media/Basket.svg','08:00 AM','01:00:00','Gratuio',3,'01/09/2016',3,NULL),(9,'BALONCESTO YUJU','ontinyent',NULL,'./backend/media/Basket.svg','08:00 AM','01:00:00','Gratuio',4,'01/09/2016',4,NULL),(10,'VOLEY JAJA','ontinyent',NULL,'./backend/media/Volei.svg','08:00 AM','01:00:00','Gratuio',4,'01/09/2016',1,NULL),(11,'VOLEY PARTIDAZO','ontinyent',NULL,'./backend/media/Volei.svg','08:00 AM','01:00:00','Gratuio',4,'01/09/2016',1,NULL),(12,'VOLEY CHICOS','ontinyent',NULL,'./backend/media/Volei.svg','08:00 AM','01:00:00','Gratuio',2,'01/09/2016',2,NULL),(13,'TENIS A TOPE','ontinyent',NULL,'./backend/media/Tenis.svg','08:00 AM','01:00:00','2.50',1,'01/08/2016',1,NULL),(14,'FOOTBALL','ontinyent',NULL,'./backend/media/Volei.svg','08:00 AM','01:00:00','2',4,'01/07/2016',4,NULL),(16,'FUTBOLIN','damian','Amery','./backend/media/Futbol.svg','08:00 AM','01:00:00','Gratuio',4,'02/20/2016',105,NULL),(17,'FUTBOLINES','damian','Amery','./backend/media/Futbol.svg','08:00 AM','01:00:00','1.50',5,'02/20/2016',105,NULL),(18,'TENIS DE SEMPRE','damian','Amery','./backend/media/Tenis.svg','08:00 AM','01:00:00','Gratuio',1,'02/20/2016',105,NULL),(19,'TENIS PER A AMATEURS','damian','Amery','./backend/media/Tenis.svg','08:00 AM','01:00:00','Gratuio',1,'02/20/2016',105,NULL),(21,'RRRRRRRRRRRRRR','damian','Amery','./backend/media/Futbol.svg','08:00 AM','01:00:00','Gratuio',4,'02/20/2016',105,NULL),(23,'RRRRRRRRRRRRRR','damian','Amery','./backend/media/Padel.svg','08:00 AM','01:00:00','Gratuio',1,'02/20/2016',105,NULL),(24,'DDDD','boxnia','Amery','./backend/media/Basket.svg','08:00 AM','01:00:00','Gratuio',1,'02/23/2016',105,NULL),(25,'TENIS PER A DOS','damian','Amery','./backend/media/Tenis.svg','08:00 AM','01:00:00','Gratuio',1,'02/20/2016',105,NULL),(26,'TENIS A PARELLES','damian','Amery','./backend/media/Tenis.svg','08:00 AM','01:00:00','Gratuio',1,'02/20/2016',105,NULL),(27,'TENIS AMB DOS','damian','Amery','./backend/media/Tenis.svg','08:00 AM','01:00:00','Gratuio',1,'02/20/2016',105,NULL),(28,'FUTBOL DE SEMPRE','damian','en la porta de darrere','./backend/media/Futbol.svg','08:00 AM','01:00:00','Gratuio',1,'02/20/2016',105,'Amery'),(29,'EEEE','boxnia','Amery','./backend/media/Futbol.svg','08:00 AM','01:00:00','Gratuio',1,'02/29/2016',105,NULL),(30,'EEE','boxnia','Amery','./backend/media/Futbol.svg','08:00 AM','01:00:00','Gratuito',1,'02/29/2016',105,NULL),(31,'DDD','boxnia','eeee','./backend/media/Basket.svg','08:00 AM','01:00:00','Gratuio',1,'02/24/2016',105,'Amery'),(32,'TENIS POLI','damian','en la puerta','./backend/media/Tenis.svg','08:00 AM','01:00:00','Gratuio',1,'02/26/2016',105,'Amery'),(33,'TENIS QUALSEVOL','DamiÃ¡n','en la porta del poli','./backend/media/Tenis.svg','08:00 AM','01:00:00','Gratuio',1,'02/27/2016',105,'Amery'),(34,'FUTBOL','damian','en la puerta trasera','./backend/media/Futbol.svg','08:00 AM','01:00:00','Gratuio',7,'02/27/2016',105,'Amery'),(35,'RRRR','ccc','dddd','./backend/media/Basket.svg','08:00 AM','01:00:00','Gratuio',1,'02/24/2016',105,'Amery'),(36,'HOLAAAAA','Vicent CortÃ©s','Nadaaaaa','./backend/media/Futbol.svg','08:00 AM','01:00:00','3',2,'02/23/2016',105,'Amery'),(37,'HOLAAAAA','Vicent CortÃ©s','dasdadada','./backend/media/Tenis.svg','12:00 AM','01:00:00','5',4,'02/23/2016',105,'Amery'),(38,'BASQUET','damian','en la porta','./backend/media/Basket.svg','08:00 AM','01:00:00','Gratuio',4,'02/27/2016',105,'Amery');
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `installation`
--

DROP TABLE IF EXISTS `installation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `installation` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ubicacion` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `descripcion` text CHARACTER SET utf8,
  `valoracion` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `avatar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `latitud` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `longitud` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `installation`
--

LOCK TABLES `installation` WRITE;
/*!40000 ALTER TABLE `installation` DISABLE KEYS */;
INSERT INTO `installation` VALUES (1,'Brett','Buggenhout','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.819381',-0.59618),(2,'Declan','Châtellerault','Lorem ipsum dolor sit amet,','Nivel.svg','installa.png','38.819716',-0.60015),(3,'Quinn','Bergen','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.810252',-0.604248),(4,'Alfonso','Lochristi','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.820384',-0.60163),(5,'Erich','Coleville Lake','Lorem','Nivel.svg','installa.png','38.821571',-0.606716),(6,'Kasper','Lamont','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.820267',-0.611844),(7,'Fuller','Fontaine-Valmont','Lorem','Nivel.svg','installa.png',' 38.825115',-0.598111),(8,'Joel','Baie-Comeau','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.988235',-1.86905),(9,'Fitzgerald','Romano d\'Ezzelino','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(10,'Vincent','Souvret','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(11,'Garth','Wyoming','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(12,'Micah','Glimes','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(13,'Tanek','Moerzeke','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(14,'Hall','Barra do Corda','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(15,'Eaton','Macklin','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(16,'Wayne','Wörgl','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(17,'Guy','Soverzene','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(18,'Armando','Valencia','Lorem ipsum','Nivel.svg','installa.png',' 39.463565 ',-0.359459),(19,'Price','Charleville-Mézières','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(20,'Timon','Buti','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(21,'Keefe','Lesve','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(22,'Seth','Meer','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(23,'Brent','Koningshooikt','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(24,'Laith','Tarvisio','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(25,'George','Sanluri','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(26,'Ulysses','Cantley','Lorem ipsum dolor sit amet,','Nivel.svg','installa.png','38.79913404632731',-0.618528),(27,'Jared','Marlborough','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(28,'Asher','Habergy','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(29,'Darius','Castlegar','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(30,'Zahir','Tournefeuille','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(31,'Harding','Caramanico Terme','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(32,'Kelly','Laramie','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(33,'Martin','San Rafael Abajo','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(34,'Moses','Rimbey','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.815101',-0.884399),(35,'Chaim','Fort Providence','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(36,'Oscar','Lourdes','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(37,'Timon','Lauregno/Laurein','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(38,'Blake','Mannheim','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(39,'Judah','Gomz?-Andoumont','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(40,'Kevin','Hoorn','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(41,'Malachi','Dillenburg','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(42,'Quamar','Valencia','Lorem ipsum','Nivel.svg','installa.png',' 38.810267',-0.604249),(43,'Tucker','Robelmont','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(44,'Bernard','Pemberton','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(45,'Hilel','Anderlecht','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(46,'Harding','Hody','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(47,'Howard','Rattray','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(48,'Chadwick','Kakinada','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(49,'Kennan','Alexandra','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(50,'Rafael','Bowden','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(51,'Theodore','Schwaz','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(52,'Sebastian','Moe','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(53,'Philip','Lampertheim','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(54,'Bevis','Torino','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(55,'Branden','Crescentino','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(56,'Jordan','Cartagena','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(57,'Bert','Wetzlar','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(58,'Ulric','Granada','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(59,'Benedict','?mamo?lu','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(60,'Nero','Shaki','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(61,'Barry','Sokoto','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(62,'Jared','Tarvisio','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(63,'Zephania','Boncelles','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(64,'Caesar','G?rompont','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(65,'Nathan','Northumberland','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(66,'Harding','Limena','Lorem ipsum dolor sit amet,','Nivel.svg','installa.png','38.79913404632731',-0.618528),(67,'Hector','Mesa','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(68,'Anthony','Ujjain','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(69,'Justin','Portobuffolè','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(70,'Steel','Hines Creek','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(71,'Dante','Cherbourg-Octeville','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(72,'Ivan','Gulfport','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(73,'Kasper','Sint-Gillis','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(74,'Addison','Amaro','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(75,'Garrett','Town of Yarmouth','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(76,'Callum','Kitchener','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(77,'Reese','Lafayette','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(78,'Honorato','Levallois-Perret','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(79,'Reese','Pelago','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(80,'Baxter','Wiekevorst','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(81,'Erasmus','Zuccarello','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(82,'Gil','Fort Wayne','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(83,'Abdul','Verrayes','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(84,'Garrett','Hantes-Wih?ries','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(85,'Fitzgerald','Gijzegem','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(86,'Solomon','Port Coquitlam','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(87,'Talon','Poederlee','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(88,'Malachi','Dilsen-Stokkem','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(89,'Tarik','Ramagundam','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(90,'Prescott','Lustenau','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(91,'Alfonso','Dreieich','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(92,'Russell','Cabano','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(93,'Raja','Rosarno','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(94,'Burton','Yellowhead County','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(95,'Rooney','Surrey','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(96,'Cadman','High Level','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(97,'Dale','Amlwch','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(98,'Amal','Treppo Carnico','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(99,'Gil','Juneau','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(100,'Amery','Rossignol','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(101,'Amery','Rossignol','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(102,'Amery','Rossignol','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(103,'Amery','Rossignol','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(104,'Amery','Rossignol','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(105,'Amery','Ontinyent','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.833277',-0.568125),(107,'WELCOME','Oliva','fitnes en general','Nivel.svg','installa.png','38.81959848512',-0.595869),(108,'PADEL INDOOR','Ontinyent','instalaciones de padel','Nivel.svg','installa.png','38.834133457532',-0.578161),(109,'LAURA_PRUEBA','Navarres','dddddd','Nivel.svg','installa.png','38.819648639637',-0.595665),(110,'CAMP DE FÃºTBOL AGRES','Agres','yeeeeee','Nivel.svg','installa.png','38.82238',-0.60882),(111,'CAMP AGRES','Agres','jjjjj','Nivel.svg','installa.png','38.82238',-0.60882);
/*!40000 ALTER TABLE `installation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `nombre` varchar(100) CHARACTER SET utf8 NOT NULL,
  `apellidos` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `direccion` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `numero` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `poblacion` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `provincia` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `todos` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `futbol` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `baloncesto` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `voleibol` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tenis` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `padel` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `nivel` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `fecha_nac` varchar(10) CHARACTER SET utf8 NOT NULL,
  `fecha_registro` varchar(10) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `pais` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `codigo` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `token` varchar(100) CHARACTER SET dec8 DEFAULT NULL,
  `status` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `tipo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masculino` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `femenino` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `bajo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `medio` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `alto` varchar(45) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('DamiÃ¡n','','','','','','0','0','0','0','0','0','','','','','2016-02-22','http://pbs.twimg.com/profile_images/693842597895409664/WQUZFNOT_normal.jpg','','4865340063','7ef680f44f4ed89f2ba1b93ada86f90d','1','normal',NULL,NULL,NULL,NULL,NULL),('laurahotmail','','','','','','0','0','0','0','0','0','','boxnia@hotmail.com','$2y$10$epXQoCto2hJpFzDqkJnHAOu27Wi4.sknDYr71kR319GQaD0iQ2f62','01-01-1981','2016-02-18','http://www.gravatar.com/avatar/c33c3e3347236d1e200e261334ecedccc33c3e3347236d1e200e261334ecedcc?s=80&d=identicon&r=g','','','618b9a7ca1325812b34c978fdb11d2c9','0','normal',NULL,NULL,NULL,NULL,NULL),('ccc','','','','','','0','0','0','0','0','0','','kikesavall@gmail.com','$2y$10$5h1HQrE2p400P7r/9Anq2e22ciMTAlzLL2WBzyryHmjQyl2vXbEwu','02-10-1981','2016-02-22','http://www.gravatar.com/avatar/6ccf02e685c543e12c57327a75ef70236ccf02e685c543e12c57327a75ef7023?s=80&d=identicon&r=g','','','db4df2818d1886106975b65d78739218','0','normal',NULL,NULL,NULL,NULL,NULL),('cd','','','','','','0','0','0','0','0','0','','ld@gmail.com','','02-03-1981','2016-02-22','http://www.gravatar.com/avatar/81b9c14df6e456314ae8761104a89dd481b9c14df6e456314ae8761104a89dd4?s=80&d=identicon&r=g','Spain','','1e80980bb5ff1bd209b18c5f41ef627a','0','normal','','','','',''),('NFU','','','625860765','','','0','0','0','0','0','0','','nfunosfaltauno@gmail.com','','01-01-1981','2016-02-22','http://pbs.twimg.com/profile_images/681865564659302401/EJTb4ZSa_normal.png','Spain','4644441855','e117a15f9cfec0c892206077ac12898a','1','normal','','','false','true','false'),('nicklaus_','','','','Ontinyent','Valencia','0','0','0','0','1','0','','pedcremo@gmail.com','','02-16-1977','2016-02-22','http://pbs.twimg.com/profile_images/1139587464/jo_normal.jpeg','Spain','7855032','2681208a1bff2e3ef2b7ac4684022b20','1','normal','true','false','','',''),('damian','','','','Ontinyent','Valencia','0','1','0','0','1','0','','tutramite.internet@gmail.com','$2y$10$8eeAJycVV.HR6KM7Dss4ZeOZTfKzooTI2suE.TCm4cNzRxLUIj7mG','08-29-1987','2016-02-18','http://www.gravatar.com/avatar/f7e8ed2541393fc28f83de50dcc7b8fdf7e8ed2541393fc28f83de50dcc7b8fd?s=80&d=identicon&r=g','Spain','','043bd6439e609c9d72b9c9573ee88db2','1','normal','true','false','false','true','false'),('Vicent','','','678043379','Agra','Albacete','0','1','0','1','0','0','','vicentcortez@gmail.com','','02-03-1988','2016-02-21','http://www.gravatar.com/avatar/fbc24e4b23d8c18bed09d3ce7ddb8bc5fbc24e4b23d8c18bed09d3ce7ddb8bc5?s=80&d=identicon&r=g','Spain','','67266c31018c11004afc02c3bab0fa78','1','normal','true','false','false','true','false'),('vieslo','','','','','','1','1','1','1','1','1','','vieslo82@gmail.com','$2y$10$p8D2qSWoJkCgvtzw0ikFWecybvgXIedmEPQSVYUXV9iEuz2/Zcv4m','02-18-1981','2016-02-09','http://www.gravatar.com/avatar/dda579a1ee7798ee3f8e9dd69cd3cb4ddda579a1ee7798ee3f8e9dd69cd3cb4d?s=80&d=identicon&r=g','','','1e229e76f6728cfc85d4abc54e74e9e7','0','normal','true',NULL,'true',NULL,NULL),('xx','','','679485268','','','1','0','0','0','0','1','','xxx@gmail.com','','01-01-1981','2016-02-18','http://www.gravatar.com/avatar/8b59dee7e59ad63ca309edd9effc26188b59dee7e59ad63ca309edd9effc2618?s=80&d=identicon&r=g','Spain','','8e25e53bcee49c6b8214a3ae0e2f3d28','0','normal','','','false','false','true'),('xxx','','','','','','0','0','0','0','0','0','','xxxx@gmail.com','$2y$10$bUixj.LsKeeO/17aF2mpC.x2E7seB/Z5SUlRRyLz4p9wAa7jZyYUO','01-01-1981','2016-02-18','http://www.gravatar.com/avatar/580fd0f72a320a18cd393155932b5753580fd0f72a320a18cd393155932b5753?s=80&d=identicon&r=g','','','e9ad58526355099058c800627b6816d9','0','normal',NULL,NULL,NULL,NULL,NULL),('ss','','','','','','0','0','0','0','0','0','','xxxxxx@gmail.com','$2y$10$c3S9MJcaiI6dtflkVORV1.GbvOFSMQIdp6W.09RAL1roN5W4vUnra','01-01-1981','2016-02-18','http://www.gravatar.com/avatar/fbc24e4b23d8c18bed09d3ce7ddb8bc5fbc24e4b23d8c18bed09d3ce7ddb8bc5?s=80&d=identicon&r=g','','','2b67a0da0cb1921c612298e8568e1c13','0','normal',NULL,NULL,NULL,NULL,NULL),('dd','','','','','','0','0','0','0','0','0','','xxxxxxx@gmail.com','$2y$10$n6uHLYGdepmR8OQlJAbO3uL0fz2vL56Opj33Zka1H1npi.VCpMXoi','01-01-1981','2016-02-18','http://www.gravatar.com/avatar/4c746ebf3d7d55b10a0c4a3fd6b58e4c4c746ebf3d7d55b10a0c4a3fd6b58e4c?s=80&d=identicon&r=g','','','f432d1f018142ade4e6e15d4861f8d05','0','normal',NULL,NULL,NULL,NULL,NULL),('sde','','','','','','0','0','0','0','0','0','','xxxxxxxxxx@gmail.com','$2y$10$pFkKzLtesZoUbW7m6MgDAuqFkstGdw3mkQIEMT6fvC3oCU1KqVvHq','01-01-1981','2016-02-18','http://www.gravatar.com/avatar/7a291a5b7a9d9e29bb606f1ac4e551dc7a291a5b7a9d9e29bb606f1ac4e551dc?s=80&d=identicon&r=g','','','3f1931c7fb7e3cca3621a75c93415b28','0','normal',NULL,NULL,NULL,NULL,NULL),('Vicent58','','','','','','0','0','0','0','0','0','','xxxxxxxxxxxxxxxxxx@gmail.com','$2y$10$akyhpXfc8M.kuXPFAkJaoebfr5z9AE8BiluMrJNc0YvfO1f3/VOaq','02-03-1995','2016-02-21','http://www.gravatar.com/avatar/bbe9feb32d0d894dde58b4b854220293bbe9feb32d0d894dde58b4b854220293?s=80&d=identicon&r=g','','','c7848d9f306b48d2bdb2446ace2fd859','0','admin',NULL,NULL,NULL,NULL,NULL),('boxnia','','','','','','0','0','0','0','0','0','','boxnia@gmail.com','','01-01-1985','2016-02-23','http://www.gravatar.com/avatar/b3fc2137663f2249dbb40ffe9707531eb3fc2137663f2249dbb40ffe9707531e?s=80&d=identicon&r=g','Spain','','a2977cc0ab5193bfb0ca4583c253f535','0','normal','','','false','false','true'),('DamiÃ¡n DS','','','','','','0','0','0','0','0','0','','','$2y$10$ySbv5sgZCcgllBxpWxg2neOsPigr9bO.u3j.Hpf6CIbdJ05LAK7KS','','2016-02-23','https://scontent.xx.fbcdn.net/hprofile-xfa1/v/t1.0-1/c0.0.50.50/p50x50/12651382_758362847629378_947891124860350030_n.jpg?oh=cb15b7829cf36ab0befc5c1e582997dc&oe=575A5CFC','','770466516419011','2dc2830e7f35570bac5c22cb5a360649','1','normal',NULL,NULL,NULL,NULL,NULL),('laura','','','','','','0','0','0','0','0','0','','dd@hotmail.com','$2y$10$nCa0FiWUfPAGw3LkCoWrXuNZ3AbzWx73gTDvmbJuf6AzAOqulElXa','02-24-1981','2016-02-23','http://51.254.97.198/NFU/backend/media/ajax-loader.gif','Spain','','62e966644c585be7f6fe0dc9234a68d3','0','admin','','','','',''),('Mati May','','','','','','0','0','0','0','0','0','','bg@gmail.com','','01-01-1981','2016-02-23','http://51.254.97.198/NFU/backend/media/logomini.png','Spain','10153943070276112','63baa8ade63216592004a5c84d2bcdd9','1','normal','','','','','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'nfu'
--

--
-- Dumping routines for database 'nfu'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-23 23:59:25

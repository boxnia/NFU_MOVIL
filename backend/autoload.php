<?php

/* * * nullify any existing autoloads ** */
spl_autoload_register(null, false);

spl_autoload_extensions('.php,.inc.php,.class.php,.class.singleton.php');
//spl_autoload_extensions('.php,.class.php,.class.singleton.php,.inc.php,.conf.php,.conf.class.php');

spl_autoload_register('loadClasses');

function loadClasses($className) {
    $parte = explode("_", $className);
    $ruta = $parte[0];
    $ruta2 = strtoupper($parte[1]);


    if (file_exists('modules/' . $ruta . '/model/' . $ruta2 . '/' . $className . '.class.singleton.php')) {//require(BLL_USERS . "user_bll.class.singleton.php");
        set_include_path('modules/' . $ruta . '/model/' . $ruta2 . '/');
        spl_autoload($className);
    } elseif (file_exists('model/' . $className . '.class.singleton.php')) {//require(MODEL_PATH . "db.class.singleton.php");
        set_include_path('model/');
        spl_autoload($className);
    } elseif (file_exists('classes/' . $className . '.class.singleton.php')) {
        set_include_path('classes/');
        spl_autoload($className);
    } elseif (file_exists('classes/email/' . $className . '.class.singleton.php')) {
        set_include_path('classes/email/');
        spl_autoload($className);
    } elseif (file_exists('libs/PHPMailer/class.' . $className . '.php')) {
        set_include_path('libs/PHPMailer/');
        spl_autoload('class.' . $className);
    }
}

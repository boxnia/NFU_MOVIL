<?php

function debugPHP($array) {
    echo "<pre>";
    print_r($array);
    echo "</pre><br>";
}

function redirect($url) {
    die('<script>window.location.href="' . $url . '";</script>');
}

function close_session() {
    //unset ($_SESSION['user']);
    //unset ($_SESSION['msje']);
    $_SESSION = array(); // Destruye todas las variables de la sesión
    session_destroy(); // Destruye la sesión
}

function amigable($url, $return = false) {
    $amigableson = URL_AMIGABLES;
    $link = "";
    $i = 0;
    /*if ($amigableson) {
        $url = explode("&", str_replace("?", "", $url));
        foreach ($url as $key => $value) {
            $aux = explode("=", $value);
            $link .= "/" . $aux[1];
        }
    } else {*/
        $link = "app_model/index.php" . $url;
    //}
    if ($return) {
        return $link;
    }
    echo $link;
}

function image() {
    if ($_SESSION['users'])
        $link = $_SESSION['users']['avatar'];
    echo $link;
}

function get_gravatar($email, $s = 80, $d = 'monsterid', $r = 'g', $img = false, $atts = array()) {
    $email = trim($email);
    $email = strtolower($email);
    $email_hash = md5($email);

    $url = "http://www.gravatar.com/avatar/" . $email_hash;
    $url .= md5(strtolower(trim($email)));
    $url .= "?s=$s&d=$d&r=$r";
    if ($img) {
        $url = '<img src="' . $url . '"';
        foreach ($atts as $key => $val)
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}

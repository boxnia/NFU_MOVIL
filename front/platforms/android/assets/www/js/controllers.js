angular.module('app.controllers', [])

.controller('gamesCtrl', function($scope, $window, services) {

  $scope.items = [];

  data = {
    "lat_position": $window.localStorage['lat'],
    "long_position": $window.localStorage['long'],

  }
  services.post('users', 'load_coordinates_near_map', data)
    .then(function(data) {
      var items = new Array();
      var items = data.data.game;
      if (data.data.success) {
        //console.log(data.data.game);
        //  var items = new Array();



        for (var i = 0; i < items.length; i++) {

          var first_point = new google.maps.LatLng($window.localStorage['lat'], $window.localStorage['long']);
          var second_point = new google.maps.LatLng(items[i].latitud, items[i].longitud);
          var distancia = google.maps.geometry.spherical.computeDistanceBetween(first_point, second_point);
          //console.log(items[i]);
          if (distancia < 5000) {

            items[i].i = i + 1;

            //$scope.items.push({ id: $scope.items.length});
            $scope.items.push(items[i]);
            //  console.log($scope.items);


          }



        }


      } else {


      }

    });

  $scope.listlength = 10;

  $scope.loadMore = function() {
    if (!$scope.items) {
      $scope.$broadcast('scroll.infiniteScrollComplete');
      return;
    }

    if ($scope.listlength < $scope.items.length)
      $scope.listlength += 10;
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }
})

.controller('facilitiesCtrl', function($scope, services, $window) {

  $scope.items_fa = [];

  services.get('installation', 'list_installation')
    .then(function(data) {

      if (data.data.success) {
        var items_fa = new Array();

        var items_fa = data.data.nom_install;


        // console.log(data.data.nom_install[0]);
        //$scope.customers = data.data;

        console.log(items_fa.length);
        for (var i = 0; i < items_fa.length; i++) {

          var first_point = new google.maps.LatLng($window.localStorage['lat'], $window.localStorage['long']);
          var second_point = new google.maps.LatLng(items_fa[i].latitud, items_fa[i].longitud);
          var distancia = google.maps.geometry.spherical.computeDistanceBetween(first_point, second_point);

          if (distancia < 5000) {
            console.log(items_fa[i]);
            items_fa[i].i = i + 1;

            //$scope.items.push({ id: $scope.items.length});
            $scope.items_fa.push(items_fa[i]);
            //  console.log($scope.items);


          }



        }
        /*$scope.list = data.data.nom_install;

        $scope.currentPage = 1; //current page
        $scope.entryLimit = 3; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter
        $scope.totalItems = $scope.list.length;*/
        ////////////
      } else {
        $scope.messageFailure = data.data.type_error;
      }
    });


  $scope.listlength_facility = 10;

  $scope.loadMore_fa = function() {
    if (!$scope.items_fa) {
      $scope.$broadcast('scroll.infiniteScrollComplete');
      return;
    }

    if ($scope.listlength_facility < $scope.items_fa.length)
      $scope.listlength_facility += 10;
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }
})

.controller('myprofileCtrl', function($scope, services, $window, $localstorage) {


  console.log($window.localStorage['user']);


  var user = $localstorage.getObject('user');
  $scope.image = user.avatar;
  $scope.name = user.nombre;
  $scope.email = user.email;


})

.controller('loginCtrl', function($scope, $localstorage, $http, $state, $timeout, services, $cordovaGeolocation, $cordovaFacebook, $window, $ionicPopup, $ionicLoading) {

  $scope.login = {
    user: "",
    password: "",
  }
  var appID = 93713001640368;
  var version = "v2.0"; // or leave blank and default is v2.0
  $scope.facebookSignIn = function() {


    var jwt = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJjZjEzZjEwMS03ODI3LTQ4N2QtOTI5Zi1iNDQyNGI2MmUxNzQifQ.Vv1PortMkNtSWghUQiup_DszTUdMY-lKTcz20tpqH64';
    var tokens = $window.localStorage['token'];
    var profile = 'tester';

    // Build the request object
    var req = {
      method: 'POST',
      url: 'https://api.ionic.io/push/notifications',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwt
      },
      data: {
        "tokens": tokens,
        "profile": profile,
        "notification": {

          "android": {
            "message": "Hello Android!",
            "sound": "android-sound.wav",
            "icon": "ionitron.png",
            "icon_color": "#rrggbb",
            "collapse_key": "foo",
            "tag": "bar"
          },
          "ios": {
            "title": "Howdy",
            "message": "Hello iOS!"
          }
        }
      }
    };

    // Make the API call
    $http(req).success(function(resp) {
      alert(resp.data.config.notification.android.message);

      console.log("Ionic Push: Push success", resp);
    }).error(function(error) {
      // Handle error
      console.log("Ionic Push: Push error", error);
    });
  }
  var posOptions = {
    timeout: 10000,
    enableHighAccuracy: false
  };
  $cordovaGeolocation
    .getCurrentPosition(posOptions)
    .then(function(position) {
      //console.log(position);
      var lat = position.coords.latitude;
      var long = position.coords.longitude;
      $window.localStorage['lat'] = lat;
      $window.localStorage['long'] = long;


    }, function(err) {
      alert("No se ha podido localizar su ubicación")
    });


  var watchOptions = {
    timeout: 3000,
    enableHighAccuracy: false // may cause errors if true
  };

  var watch = $cordovaGeolocation.watchPosition(watchOptions);
  watch.then(
    null,
    function(error) {
      switch (error.code) {
        case error.PERMISSION_DENIED:
          $scope.error = "User denied the request for Geolocation."
          break;
        case error.POSITION_UNAVAILABLE:
          $scope.error = "Location information is unavailable."
          break;
        case error.TIMEOUT:
          $scope.error = "The request to get user location timed out."
          break;
        case error.UNKNOWN_ERROR:
          $scope.error = "An unknown error occurred."
          break;
      }
    },
    function(position) {
      var lat = position.coords.latitude
      var long = position.coords.longitude
    });


  watch.clearWatch();

  $scope.myRegex_name = /^([a-z ñáéíóú]{2,60})$/i;
  $scope.myRegex_password = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
  $scope.myRegex_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

  ////////////////////////////////////////////////login user
  ////////////////////////login user
  $scope.login_submit = function() {



    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    var SingINuser = JSON.stringify($scope.login);
    //alert(SingINuser);
    //  $window.localStorage['user'] = $scope.login.user;

    services.post('login', 'signin', SingINuser)
      .then(function(data) {

        if (data.data.success) {
          $ionicLoading.hide();
          /*  $scope.login.msg = data.data.message*/

          $localstorage.setObject('user', data.data.users);
          var authProvider = 'basic';
          var authSettings = {
            'remember': true
          };

          var loginDetails = {
            'email': data.data.users.email,
            'password': $scope.login.password,
          };

          $scope.authSuccess = function() {
            // replace dash with the name of your main state
            $state.go('mENU.games');
          };

          $scope.authFailure = function(errors) {
            for (var err in errors) {
              // check the error and provide an appropriate message
              // for your application
            }
          };

          Ionic.Auth.login(authProvider, authSettings, loginDetails)
            .then($scope.authSuccess, $scope.authFailure);



          //  $window.localStorage['user'] =JSON.stringify( data.data.users);

          //    console.log(data.data.users);
          /*  authService.SetCredentials(data.data.users);*/

          /*console.log($cookies.getObject('user'));*/
          //

        } else {

          $ionicLoading.hide();
          $scope.AlertMessage = true;
          //console.log(data.data);
          $timeout(function() {

            $scope.AlertMessage = false;

          }, 5000)

          //$scope.login.msg = data.data.error.message;
          //alert(data.data.error.user);
          $scope.login.user_error = data.data.error.user;
          $scope.login.password_error = data.data.error.password;

          $ionicPopup.alert({
            title: data.data.error.activar,
            template: 'No olvide darlo de alta en su bandeja de correo'
          });

        }

      });


  }

})

.controller('newGameCtrl', function($scope, services, $http) {

})

.controller('modifyCtrl', function($scope, services, $localstorage, $cordovaFileTransfer, $http, $cordovaImagePicker, $cordovaCamera, $localstorage, $window, $timeout, $ionicPopup, $ionicLoading) {

  var user = $localstorage.getObject('user');
  $scope.collection = {
    selectedImage: ''
  };

  var lat = $window.localStorage['lat'];
  var long = $window.localStorage['long'];
  var collectionDate = user.fecha_nac;

  $scope.modify = {
    name: user.nombre,
    telf: user.numero,
    email: user.email,
    date: new Date(collectionDate),
    sex: [{
        id: 1,
        text: "Masculino",
        value: "Male",
      }, {
        id: 2,
        text: "Femenino",
        value: "Female",
      },

    ],
    level: [{
        id: 1,
        text: "Alto",
        value: "Hight",
        checked: user.alto
      }, {
        id: 2,
        text: "Medio",
        value: "Medium",
        checked: user.medio
      }, {
        id: 3,
        text: "Bajo",
        value: "Low",
        checked: user.bajo
      },

    ],
    sport: [{
      id: 1,
      checked: user.todos,
      name: "todos"
    }, {
      id: 2,
      checked: user.futbol,
      name: "futbol"
    }, {
      id: 3,
      checked: user.baloncesto,
      name: "basket"
    }, {
      id: 4,
      checked: user.voleibol,
      name: "volei"
    }, {
      id: 5,
      checked: user.tenis,
      name: "tenis"
    }, {
      id: 6,
      checked: user.padel,
      name: "padel"
    }],

  }

  $scope.data = {
    sex: user.masculino,
    level: user.bajo,
  };
  var masculino;
  var bajo;
  $scope.newValue = function(value) {
    masculino = value
  }

  $scope.newValue_level = function(value) {
      bajo = value
    }
    /////////////////////location city

  //var parseString =xml2js.parseString;
  var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + long + '&sensor=true';

  $http.get(url).then(function(response) {
    //the response from the server is now contained in 'response'
    //  console.log(response.data.results[0]['address_components']);
    $scope.modify.city = response.data.results[0]['address_components'][2]['long_name'];
    $scope.modify.provin = response.data.results[0]['address_components'][3]['long_name'];
  }, function(error) {
    //there was an error fetching from the server
  });

  ///////////////////////////////////////////upload image
  ///////////////////////////////////////////////take foto

  $scope.imgURI = user.avatar;


  $scope.takePhoto = function() {
    var options_img = {
      fileKey: "avatar",
      fileName: "image.png",
      chunkedMode: "false",
      mimeType: "false",
      quality: 75,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false
    };
    $cordovaCamera.getPicture(options_img).then(function(imageData) {
      $scope.imgURI = "data:image/jpeg;base64," + imageData;
      $scope.img = imageData;
    }, function(err) {
      // An error occured. Show a message to the user
    });


    //var img = document.getElementById('image');
    var imageURI = $scope.img;
    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";
    var params = new Object();
    options.params = params;
    options.chunkedMode = false;
    var ft = new FileTransfer();
    ft.upload(imageURI, "http://51.254.97.198/NFU/backend/index.php?module=users&function=upload_users", win, fail,
      options);

    function win(r) {
      console.log("Code = " + r.responseCode);
      console.log("Response = " + r.response);
      console.log("Sent = " + r.bytesSent);
    }

    function fail(error) {
      alert("An error has occurred: Code = " + error.code);
      console.log("upload error source " + error.source);
      console.log("upload error target " + error.target);
    }

  };
  /*$scope.gallery = function() {
      var options = {
        maximumImagesCount: 10,
        width: 300,
        height: 300,
        quality: 75
      };

      $cordovaImagePicker.getPictures(options)
        .then(function(results) {
          for (var i = 0; i < results.length; i++) {
            console.log('Image URI: ' + results[i]);

            $scope.imgURI = results[i];
            $scope.img=results[i];
          }
        }, function(error) {
          // error getting photos
        });
    //  var img = document.getElementById('image');
      var imageURI =   $scope.img;
      var options = new FileUploadOptions();
      options.fileKey = "file";
      options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
      options.mimeType = "image/jpeg";
      var params = new Object();
      options.params = params;
      options.chunkedMode = false;
      var ft = new FileTransfer();
      ft.upload(imageURI, "http://51.254.97.198/NFU/backend/index.php?module=users&function=upload_users", win, fail,
        options);

      function win(r) {
        console.log("Code = " + r.responseCode);
        console.log("Response = " + r.response);
        console.log("Sent = " + r.bytesSent);
      }

      function fail(error) {
        alert("An error has occurred: Code = " + error.code);
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
      }
    }*/

  $scope.gallery = function() {
      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imageData) {

        //console.log(imageData);
        //console.log(options);
        var image = document.getElementById('tempImage');
        $scope.imageURI = imageData;
        $scope.img = imageData;
        alert(imageData);
        var server = "http://51.254.97.198/backend/utils/upload.php",
          filePath = imageData;

        var date = new Date();

        var options = {
          fileKey: "file",
          fileName: imageData.substr(imageData.lastIndexOf('/') + 1),
          chunkedMode: false,
          mimeType: "image/jpg"
        };

        $cordovaFileTransfer.upload(server, filePath, options).then(function(result) {
          alert("SUCCESS: " + JSON.stringify(result.response));
          alert('Result_' + result.response[0] + '_ending');
          alert("success");
          alert(JSON.stringify(result.response));

        }, function(err) {
          console.log("ERROR: " + JSON.stringify(err));
          //alert(JSON.stringify(err));
        }, function(progress) {
          // constant progress updates
        });


      }, function(err) {
        // error
        alert(err);
      });
    }
    ////////////////////////////////////////////////update user admin
  $scope.update = function() {


    var poblacion;


    var deporte = new Array();
    var j = 0;
    for (var i = 0; i < $scope.modify.sport.length; i++) {
      if ($scope.modify.sport[i].checked === true) {
        deporte[j] = $scope.modify.sport[i].name;
        j++;

      }
    }



    if (!bajo) {
      bajo = false;
    }
    var n = formatDate($scope.modify.date)
    console.log(n)


    var data = {
      "nombre": $scope.modify.name,
      "provincia": $scope.modify.provin,
      "poblacion": $scope.modify.city,
      "masculino": masculino,
      "femenino": masculino,
      "telefono": $scope.modify.telf,
      "email": $scope.modify.email,
      //  "password_bd": user.password,
      "bajo": bajo,
      "medio": bajo,
      "alto": bajo,
      //"password2": $scope.profile.password2,
      "fecha_nac": n,
      "fecha_registro": user.fecha_registro,
      //  "password_old": $scope.profile.password_old,
      "status": user.status,
      "token": user.token,
      "tipo": user.tipo,
      "deporte": deporte,
      "avatar": $scope.img,
    }


    console.log(data);
    var datos = JSON.stringify(data)
      //console.log(datos);
    services.put('users', 'update_users', datos)
      .then(function(data) {
        $ionicLoading.show();
        console.log(data)
        if (data.data.success) {
          ///////////////actualizar localstorage
          $localstorage.setObject('user', data.data.users);
          $ionicLoading.hide();

          console.log(data)
          var confirmPopup = $ionicPopup.confirm({
            title: 'Sus datos se han modificado con éxito',

          });

          confirmPopup.then(function(res) {
            if (res) {
              //  console.log('ok');
              $state.go('mENU.myprofile');
            } else {
              //console.log('You are not sure');
            }
          });


        } else {
          $ionicLoading.hide();
          $scope.AlertMessage = true;
          $timeout(function() {

            $scope.AlertMessage = false;

          }, 5000)
          $scope.modify.date_error = data.data.error.fecha_nac;
          $scope.modify.avatar = data.data.error_avatar;
          /*   $scope.modify.email_error = data.data.error.email;
            $scope.modify.password_error = data.data.error.password;
            $scope.modify.password2_error = data.data.error.password2;
            $scope.modify.password_old_error = data.data.error.password_old;
            $scope.modify.password_old_telefono = data.data.error.telefono;*/
          if (data.data.type_error === "503") {
            var confirmPopup = $ionicPopup.confirm({
              title: 'Oops... Error en la actualización de la bd',

            });
            //  sweetAlert("Oops... Error en la actualización de la bd");

          }
        }
      });



  }

  function formatDate(value) {
    return value.getDate() + "-" + (value.getMonth() + 1) + "-" + value.getYear();
  }







})

.controller('newfacilityNCtrl', function($scope, services) {

})

.controller('mapsGameCtrl', function($scope, services, $cordovaGeolocation) {

  var options = {
    timeout: 10000,
    enableHighAccuracy: true
  };

  $cordovaGeolocation.getCurrentPosition(options).then(function(position) {

    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    var mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

  }, function(error) {
    console.log("Could not get location");
  });

})

.controller('signupCtrl', function($scope, services, $filter, $timeout, $ionicPopup, $state, $ionicLoading) {


  $scope.myRegex_name = /^([a-z ñáéíóú]{2,60})$/i;
  $scope.myRegex_password = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
  $scope.myRegex_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
  $scope.signup = {
    name: "",
    password: "",
    password2: "",
    email: "",
    dateOfBirth: "",
  };

  //  console.log($scope.signup);

  $scope.altaUser = function() {

    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    var data = {

      nombre: $scope.signup.name,
      password: $scope.signup.password,
      password2: $scope.signup.password2,
      email: $scope.signup.email,
      fecha_nac: $filter('date')($scope.signup.dateOfBirth, 'MM-dd-yyyy'),
      fecha_registro: $filter('date')(new Date(), 'yyyy-MM-dd'),
      fecha: "",
    }

    var details = {
      'name': $scope.signup.name,
      'email': $scope.signup.email,
      'password': $scope.signup.password
    }
    console.log(details);
    // optionally pass a username
    // details.username = 'ionitron';

    Ionic.Auth.signup(details);
    //console.log(data);
    var user = JSON.stringify(data);
    //alert(user);
    services.post('login', 'alta_users', user)

    .then(function(data) {

      if (data.data.success) {
        //  console.log(data.data);
        $ionicLoading.hide();


        var confirmPopup = $ionicPopup.confirm({
          title: 'El usuario se ha registrado con éxito',
          template: 'Para finalizar el alta revise su bandeja de correo'
        });

        confirmPopup.then(function(res) {
          if (res) {
            //  console.log('ok');
            $state.go('login');
          } else {
            //console.log('You are not sure');
          }
        });

        /*$timeout(function () {
            $location.path('/admin');
        }, 6000);*/

      } else {
        $ionicLoading.hide();
        $scope.AlertMessage = true;
        //  console.log(data.data);
        $scope.signup.fecha = data.data.error.fecha_nac;
        $scope.signup.msg = data.data.error.message;
        $scope.signup.name_error = data.data.error.nombre;
        $scope.signup.email_error = data.data.error.email;
        $timeout(function() {

          $scope.AlertMessage = false;

        }, 5000)
      }

    });
  }


})

.controller('recoveryPasswordCtrl', function($scope, services, $ionicPopup, $state, $ionicLoading) {

  $scope.myRegex_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

  $scope.recovery = {
    email: "",
  };


  $scope.recovery_submit = function() {
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    var data = {
        email: $scope.recovery.email
      }
      //  console.log(data);
      /*var recovery_email_JSON = JSON.stringify(data);*/
      /*console.log("estamos dentro de la funcion de recovery en angular linea 194 "+recovery_email_JSON);*/
      //console.log("estamos dentro de la funcion de recovery en angular linea 194 ");
    services.post('login', 'email_recovery', data).then(function(data) {

      if (data.data.success) {

        /*//////////////////fail*/
        //console.log(data);
        //console.log(data.data.mensaje);
        $ionicLoading.hide();


        var confirmPopup = $ionicPopup.confirm({
          title: data.data.mensaje,

        });

        confirmPopup.then(function(res) {
          if (res) {
            //  console.log('ok');
            $state.go('login');
          } else {
            //  console.log('You are not sure');
          }
        });

        //console.log("estamos dentro de la funcion de recovery en angular linea 202");

        /*$("#sed_email_recovery").focus().after("<div class='error'>" + data.mensaje + "</div>");*/

      } else {
        $ionicLoading.hide();
        /*//////////////////ok*/
        //console.log("estamos dentro de la funcion de recovery en angular linea 207")
        //  console.log(data.data);
        $scope.recovery.recov_email_error = data.data.mensaje;
        /*swal(data.data.mensaje);*/

        /*$timeout(function () {
         $location.path('/recovery_pass');
         }, 3000);/*a utilizar esta*/
        //console.log("estamos dentro de la funcion de recovery en angular linea 210")

      }

    });

  }
})

.controller('gameCtrl', function($scope, services, $stateParams, $ionicLoading) {
  //console.log($stateParams);
  $ionicLoading.show();
  services.get('games', 'idGame', $stateParams.id).then(function(data) {
    var user = Ionic.User.current();
    console.log(user);
    console.log(user.isAuthenticated())
    if (user.isAuthenticated()) {
      // user is logged in
    } else {
      // user is NOT logged in
    }
    if (data.data.success) {
      $ionicLoading.hide();
      /*//////////////////fail*/
      //    console.log(data.data.game);
      var game = data.data.game;
      $scope.image = game.deporte;
      //console.log(data.data.mensaje);
      $scope.hour = game.hora;
      $scope.day = game.dia;



    } else {
      $ionicLoading.hide();
      /*//////////////////ok*/
      //console.log("estamos dentro de la funcion de recovery en angular linea 207")
      //console.log(data.data);
      $scope.recovery.recov_email_error = data.data.mensaje;
      /*swal(data.data.mensaje);*/

      /*$timeout(function () {
       $location.path('/recovery_pass');
       }, 3000);/*a utilizar esta*/
      //console.log("estamos dentro de la funcion de recovery en angular linea 210")

    }

  });

})

.controller('settingsCtrl', function($scope, services) {

})

.controller('usersCtrl', function($scope, services) {

})

.controller('locationCtrl', function($scope, services) {

})

.controller('chatCtrl', function($scope, services) {

})

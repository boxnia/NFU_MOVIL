angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider



    .state('mENU.games', {
    url: '/games',
    views: {
      'side-menu21': {
        templateUrl: 'templates/games.html',
        controller: 'gamesCtrl'
      }
    }
  })

  .state('mENU.facilities', {
    url: '/facilities',
    views: {
      'side-menu21': {
        templateUrl: 'templates/facilities.html',
        controller: 'facilitiesCtrl'
      }
    }
  })

  .state('mENU.myprofile', {
    url: '/myprofile',
    views: {
      'side-menu21': {
        templateUrl: 'templates/myprofile.html',
        controller: 'myprofileCtrl'
      }
    }
  })

  .state('mENU', {
    url: '/menu',
    templateUrl: 'templates/mENU.html',
    abstract: true
  })

  .state('login', {
    url: '/',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('mENU.newGame', {
    url: '/newgame',
    views: {
      'side-menu21': {
        templateUrl: 'templates/newGame.html',
        controller: 'newGameCtrl'
      }
    }
  })

  .state('mENU.modify', {
    url: '/modify',
    views: {
      'side-menu21': {
        templateUrl: 'templates/modify.html',
        controller: 'modifyCtrl'
      }
    }
  })

  .state('mENU.newfacility', {
    url: '/newfacility',
    views: {
      'side-menu21': {
        templateUrl: 'templates/newfacility.html',
        controller: 'newfacilityCtrl'
      }
    }
  })

  .state('mapGamesCtrl', {
    url: '/map',
    templateUrl: 'templates/mapGames.html',
    controller: 'mapaPartidasCtrl'
  })

  .state('signup', {
    url: '/singup',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('recover', {
    url: '/recovery',
    templateUrl: 'templates/recoverPassword.html',
    controller: 'recoveryPasswordCtrl'
  })

  .state('partida', {
    url: '/game/:id',
    templateUrl: 'templates/game.html',
    controller: 'gameCtrl'
  })

  .state('mENU.settings', {
    url: '/settings',
    views: {
      'side-menu21': {
        templateUrl: 'templates/settings.html',
        controller: 'settingsCtrl'
      }
    }
  })

  .state('users', {
    url: '/usersplay',
    templateUrl: 'templates/users.html',
    controller: 'usersCtrl'
  })

  .state('location', {
    url: '/location',
    templateUrl: 'templates/location.html',
    controller: 'locationCtrl'
  })

  .state('chat', {
    url: '/chat',
    templateUrl: 'templates/chat.html',
    controller: 'chatCtrl'
  })

  $urlRouterProvider.otherwise('/')



});

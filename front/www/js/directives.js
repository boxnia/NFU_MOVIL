var app=angular.module('app.directives', [])

/////////////////////////validar password


app.directive('passwordCheck', [function () {
        return {
            require: 'ngModel',
            link: function (scope,  element, attrs, ctrl) {
                var firstPassword = '#' + attrs.passwordCheck;
                //  console.info(firstPassword);
                $(element).add(firstPassword).on('keyup', function () {
                    scope.$apply(function () {
                      //  console.info($(element).val() === $(firstPassword).val());
                        ctrl.$setValidity('pwmatch', $(element).val() === $(firstPassword).val());
                    });
                });
            }
        }
    }]);

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('app', ['ionic', 'app.controllers', 'ionic.service.push', 'app.routes', 'app.services', 'app.directives', 'ngCordova'])

.run(function($ionicPlatform, $window) {
  $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    var user = Ionic.User.current();
    console.log(user);
    var settings = new Ionic.IO.Settings();
    var app_id = settings.get('app_id');
    //  console.log(app_id);
   var push = new Ionic.Push({
      "debug": true
    });

    push.register(function(token) {
      console.log("My Device token:", token.token);
      $window.localStorage['token'] = token.token
      //$("#gcm_id").val(token.token);
      push.addTokenToUser(user);

      push.saveToken(token); // persist the token in the Ionic Platform
      user.save();
    });
    var pushNotification = PushNotification.init({
      android: {
        "android": {
          "senderID": "630829998598"
        }
      }
    });

    //Just for demonstration
    pushNotification.on('notification', function(data) {
      alert(data.message);
      $("#gcm_id").val(data.message);
      alert(data.title);
      alert(data.count);
      alert(data.sound);
      alert(data.image);
      alert(data.additionalData);
    });




  });
})
